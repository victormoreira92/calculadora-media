let adicionar = document.querySelector('.btn')
let calcular = document.querySelector('#btn_calcular')
let areatexto = document.querySelector('.areatexto')
let notas = document.querySelector('.notas')
let res = document.querySelector('.resultado')

let listaNotas = []

adicionar.addEventListener('click',(e) =>{
    if(!notas.value){
        alert("Por favor, insira uma nota")
        notas.value = ''

    }else if(!parseInt(notas.value)){
        alert("A nota digitada é inválida, por favor, insira uma nota válida")
        notas.value = ''

    }else if(parseFloat(notas.value) > 10 || parseFloat(notas.value) < 0){
        alert("A nota digitada é inválida, por favor, insira uma nota válida")
        notas.value = ''
    }else{
        listaNotas.push(parseFloat(notas.value.replace(/,/g, '.')))
        let mensagem = "A nota "+listaNotas.length+" foi "+listaNotas[listaNotas.length - 1]+"\n"
        areatexto.value += mensagem
        notas.value = ''
    }
})

calcular.addEventListener('click',()=>{
    let media = listaNotas.reduce((soma, i) =>{
        return soma + i
    })/listaNotas.length

    res.textContent = " A média é "+media.toFixed(2)

})